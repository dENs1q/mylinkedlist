package com.company.collections;


import com.company.collections.mylinkedlist.ILinkedList;
import com.company.collections.mylinkedlist.MyLinkedList;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MainClass {
    public static void main(String[] args) {

        ILinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(5);
        System.out.println("myLinkedList = " + myLinkedList);
        myLinkedList.add(7);
        System.out.println("myLinkedList = " + myLinkedList);
        myLinkedList.add(6);
        System.out.println("myLinkedList = " + myLinkedList);
        System.out.println("myLinkedList.get(1) = " + myLinkedList.get(1));
        myLinkedList.add(1, 10);
        System.out.println("myLinkedList = " + myLinkedList);
        System.out.println("myLinkedList.get(1) = " + myLinkedList.get(1));
        System.out.println("myLinkedList.get(1) = " + myLinkedList.get(2));
        System.out.println("myLinkedList.remove(1) = " + myLinkedList.remove(1));
        System.out.println("myLinkedList = " + myLinkedList);
        System.out.println("myLinkedList.get(1) = " + myLinkedList.get(1));
        System.out.println("myLinkedList.indexOf(10) = " + myLinkedList.indexOf(10));
        System.out.println("myLinkedList.indexOf(7) = " + myLinkedList.indexOf(7));
        System.out.println("myLinkedList.set(1, 8) = " + myLinkedList.set(1, 8));
//        myLinkedList.clear();
        System.out.println("myLinkedList = " + myLinkedList);
        Integer[] myArray = new Integer[10];
        System.out.println(Arrays.toString(myLinkedList.toArray(myArray)));
//        for (Object o : myLinkedList.toArray()) {
//            System.out.print(" " + o);
//        }

//        Test time MyLinkedList
        java.util.LinkedList<Integer> list = new java.util.LinkedList<>();
        MyLinkedList<Integer> myList = new MyLinkedList<>();

        long startList = System.nanoTime();
        for (int i = 0; i < 100001; i++) {
            list.add(i);
        }
        long finishList = System.nanoTime();

        long startMyList = System.nanoTime();
        for (int i = 0; i < 100001; i++) {
            myList.add(i);
        }
        long finishMyList = System.nanoTime();

        System.out.println("time add LinkedList: " + ((finishList - startList) / 1000000) + " ms");
        System.out.println("time add myLinkedList: " + ((finishMyList - startMyList) / 1000000) + " ms");

        startList = System.nanoTime();
        list.remove(50000);
        finishList = System.nanoTime();

        startMyList = System.nanoTime();
        myList.remove(50000);
        finishMyList = System.nanoTime();

        System.out.println("time get LinkedList: " + ((finishList - startList) / 1000000) + " ms");
        System.out.println("time get myLinkedList: " + ((finishMyList - startMyList) / 1000000) + " ms");

    }
}
