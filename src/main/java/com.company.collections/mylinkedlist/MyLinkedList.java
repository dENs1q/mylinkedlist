package com.company.collections.mylinkedlist;

import java.util.Iterator;

public class MyLinkedList<E> implements ILinkedList<E> {

    private Node<E> firstElement;
    private Node<E> lastElement;
    private int size;


    @Override
    public void add(E element) {
        Node<E> node = new Node<>(element);
        if (size == 0) {
            firstElement = node;
            lastElement = firstElement;
            size++;
            return;
        }
        lastElement.setNextNode(node);
        lastElement = node;
        size++;
    }

    @Override
    public void add(int index, E element) {
        myIndexOutOfBoundsException(index);
        if ((index == 0 && size == 0) || index == size) {
            add(element);
            return;
        }
        if (index == 0) {
            firstElement = new Node<>(element, firstElement);
            size++;
            return;
        }
        Node<E> runner = getNode(index - 1);
        Node<E> elem = new Node<>(element, runner.getNextNode());
        runner.setNextNode(elem);
        size++;
    }

    @Override
    public void clear() {
        for (Node<E> node = firstElement; node != null; ) {
            Node<E> nextNode = node.getNextNode();
            node.setElement(null);
            node.setNextNode(null);
            node = nextNode;
        }
        firstElement = null;
        lastElement = null;
        size = 0;
    }

    @Override
    public E get(int index) {
        return getNode(index).getElement();
    }

    private Node<E> getNode(int index) {
        myIndexOutOfBoundsException(index);
        Node<E> runner = firstElement;
        for (int i = 0; i < index; i++) {
            runner = runner.getNextNode();
        }
        return runner;
    }

    @Override
    public int indexOf(E element) {
        Node<E> node;
        for (int index = 0; index < size(); index++) {
            node = getNode(index);
            if (node.getElement().equals(element)) {
                return index;
            }
        }
        return -1;
    }

    @Override
    public E remove(int index) {
        myIndexOutOfBoundsException(index);
        Node<E> preNode = getNode(index - 1);
        Node<E> curNode = getNode(index);
        Node<E> nextNode = getNode(index + 1);
        preNode.setNextNode(nextNode);
        E element = curNode.getElement();
        curNode.setNextNode(null);
        curNode.setElement(null);
        size--;
        return element;
    }

    @Override
    public E set(int index, E element) {
        myIndexOutOfBoundsException(index);
        Node<E> node = getNode(index);
        E oldElement = node.getElement();
        node.setElement(element);
        return oldElement;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E[] toArray(E[] array) {
        if (array.length < size) {
            array = (E[]) java.lang.reflect.Array.newInstance(array.getClass().getComponentType(), size);
        }
        Node<E> current = firstElement;
        for (int i = 0; i < size; i++) {
            array[i] = current.getElement();
            current = current.getNextNode();
        }

        if (array.length > size) {
            array[size] = null;
        }
        return array;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyIterator();
    }

    @Override
    public String toString() {
        return "MyLinkedList{" +
                "firstElement=" + firstElement +
                ", lastElement=" + lastElement +
                ", size=" + size +
                '}';
    }

    // Checking the index
    private void myIndexOutOfBoundsException(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of range");
        }
    }

    private class MyIterator implements Iterator<E> {
        private Node<E> current;

        @Override
        public boolean hasNext() {
            if (firstElement != null && current == null) {
                return true;
            }
            return current.getNextNode() != null;
        }

        @Override
        public E next() {
            if (current == null) {
                current = firstElement;
            } else {
                current = current.getNextNode();
            }
            return current.getElement();
        }
    }
}
