package com.company.collections;

import java.util.*;

public class MainTest {
    public static void main(String[] args) {
        HashMap hashMap = new HashMap();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        TreeMap treeMap = new TreeMap();

        for (int i = 0; i < 100001; i++) {
            hashMap.put(i, i);
        }
        long startHashMap = System.nanoTime();
        hashMap.remove(50000);
        long finishHashMap = System.nanoTime();
        System.out.println("hashSet " + ((finishHashMap - startHashMap) ) + " ms");

        for (int i = 0; i < 100001; i++) {
            linkedHashMap.put(i, i);
        }
        long startLinkedHashMap = System.nanoTime();
        linkedHashMap.remove(50000);
        long finishLinkedHashMap = System.nanoTime();
        System.out.println("linkedHashSet " + ((finishLinkedHashMap - startLinkedHashMap) ) + " ms");

        for (int i = 0; i < 100001; i++) {
            treeMap.put(i, i);
        }
        long startTreeMap = System.nanoTime();
        treeMap.remove(50000);
        long finishTreeMap = System.nanoTime();
        System.out.println("treeSet " + ((finishTreeMap - startTreeMap) ) + " ms");
    }
}
