test time

a) ArrayList vs. LinkedList
100000 elements
	1) add
		win LinkedList

	2) get 
		win ArrayList

	3) remove 
		win ArrayList

b) HashSet vs. LinkedHashSet vs. TreeSet 
100000 elements
	1) add
		win LinkedHashSet

	2) contains
		win linkedHashSet

	3) remove
		win linkedHashSet

c) HashMap vs. LinkedHashMap vs. TreeMap
100000 elements
	1) add
		win HashMap

	2) get
		win LinkedHashMap

	3) remove 
		win LinkedHashMap
